<?php

namespace Drupal\plasmic\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

class PlasmicController extends ControllerBase {

  public function contentList() {
    $header = [
      ['data' => $this->t('ID'), 'field' => 'id'],
      ['data' => $this->t('Title'), 'field' => 'title'],
      ['data' => $this->t('Type'), 'field' => 'type'],
      ['data' => $this->t('Operations'), 'field' => 'operations'],
    ];

    $rows = [];
    // Assuming you have two separate entity types for full page and content
    $entity_types = ['plasmic_full_page', 'plasmic_content'];

    foreach ($entity_types as $entity_type) {
      $entities = $this->entityTypeManager()->getStorage($entity_type)->loadMultiple();
      foreach ($entities as $entity) {
        $row = [];
        $row[] = $entity->id();

        $row[] = Link::fromTextAndUrl($entity->label(), $entity->toUrl());
        $row[] = $entity->getEntityType()->getLabel();

        // Operations (view, edit, delete links)
        $links = [];
        $links['view'] = [
          'title' => $this->t('View'),
          'url' => $entity->toUrl('canonical'),
        ];
        $links['edit'] = [
          'title' => $this->t('Edit'),
          'url' => $entity->toUrl('edit-form'),
        ];
        $links['delete'] = [
          'title' => $this->t('Delete'),
          'url' => $entity->toUrl('delete-form'),
        ];

        $row[] = [
          'data' => [
            '#type' => 'operations',
            '#links' => $links,
          ],
        ];

        $rows[] = $row;
      }
    }

    // Add links to create new entities
    $build['add_full_page'] = [
      '#type' => 'link',
      '#title' => $this->t('Add Plasmic Full Page'),
      '#url' => Url::fromRoute('plasmic.full_page.add'),
      '#attributes' => ['class' => ['button', 'button--primary']],
    ];

    $build['add_content'] = [
      '#type' => 'link',
      '#title' => $this->t('Add Plasmic Content'),
      '#url' => Url::fromRoute('entity.plasmic_content.add_form'),
      '#attributes' => ['class' => ['button', 'button--primary']],
    ];

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No plasmic entities found.'),
    ];

    return $build;
  }
}
