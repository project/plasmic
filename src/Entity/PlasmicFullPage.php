<?php
namespace Drupal\plasmic\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Plasmic Full Page entity.
 *
 * @ContentEntityType(
 *   id = "plasmic_full_page",
 *   label = @Translation("Plasmic Full Page"),
 *   base_table = "plasmic_full_page",
 *   admin_permission = "administer plasmic full page entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *   },
 *   links = {
 *     "canonical" = "/plasmic/full/{plasmic_full_page}",
 *     "add-form" = "/plasmic/full/add",
 *     "edit-form" = "/plasmic/full/{plasmic_full_page}/edit",
 *     "delete-form" = "/plasmic/full/{plasmic_full_page}/delete",
 *     "collection" = "/admin/content/plasmic"
 *   },
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "form" = {
 *       "default" = "Drupal\plasmic\Form\PlasmicFullPageForm",
 *       "add" = "Drupal\plasmic\Form\PlasmicFullPageForm",
 *       "edit" = "Drupal\plasmic\Form\PlasmicFullPageForm",
 *       "delete" = "Drupal\plasmic\Form\PlasmicFullPageForm",
 *     },
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 * )
 */

class PlasmicFullPage extends ContentEntityBase {
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the Plasmic Full Page.'))
      ->setRequired(TRUE)
      ->setSettings(['max_length' => 255])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ]);

    $fields['plasmic_component_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Plasmic Component ID'))
      ->setDescription(t('The ID of the Plasmic component to render.'))
      ->setRequired(TRUE)
      ->setSettings(['max_length' => 255])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ]);

    // Automatically handled by the path module if entity is properly configured
    // If manual handling needed:
    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL Alias'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 50,
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }
}
