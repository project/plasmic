<?php
namespace Drupal\plasmic\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Plasmic Content entity.
 *
 * @ContentEntityType(
 *   id = "plasmic_content",
 *   label = @Translation("Plasmic Content"),
 *   base_table = "plasmic_content",
 *   admin_permission = "administer Plasmic Content entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *   },
 *   links = {
 *     "canonical" = "/plasmic/content/{plasmic_content}",
 *     "add-form" = "/plasmic/content/add",
 *     "edit-form" = "/plasmic/content/{plasmic_content}/edit",
 *     "delete-form" = "/plasmic/content/{plasmic_content}/delete",
 *     "collection" = "/admin/content/plasmic"
 *   },
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "form" = {
 *       "default" = "Drupal\plasmic\Form\PlasmicContentForm",
 *       "add" = "Drupal\plasmic\Form\PlasmicContentForm",
 *       "edit" = "Drupal\plasmic\Form\PlasmicContentForm",
 *       "delete" = "Drupal\plasmic\Form\PlasmicContentForm",
 *     },
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 * )
 */

class PlasmicContent extends ContentEntityBase {
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the Plasmic Content.'))
      ->setRequired(TRUE)
      ->setSettings(['max_length' => 255])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ]);

   /* $fields['plasmic_component_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Plasmic Component ID'))
      ->setDescription(t('The ID of the Plasmic component to render.'))
      ->setRequired(TRUE)
      ->setSettings(['max_length' => 255])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ]);*/

    // PlasmicComponent custom field definition
    $fields['plasmic_component'] = BaseFieldDefinition::create('plasmic_component')
      ->setLabel(t('Plasmic Component'))
      ->setDescription(t('Select a Plasmic component to render.'))
      ->setDisplayOptions('form', [
        'type' => 'plasmic_component_default',
        'weight' => 1,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'plasmic_component_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Automatically handled by the path module if entity is properly configured
    // If manual handling needed:
    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL Alias'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 50,
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }
}
