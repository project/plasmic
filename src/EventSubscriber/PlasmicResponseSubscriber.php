<?php

namespace Drupal\plasmic\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Drupal\plasmic\PlasmicService;
use Drupal\Core\Cache\CacheBackendInterface;

class PlasmicResponseSubscriber implements EventSubscriberInterface {

  /**
   * The Plasmic service.
   *
   * @var \Drupal\plasmic\PlasmicService
   */
  protected $plasmicService;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Constructs a new PlasmicResponseSubscriber.
   *
   * @param \Drupal\plasmic\PlasmicService $plasmicService
   *   The Plasmic service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   The cache backend interface.
   */
  public function __construct(PlasmicService $plasmicService, CacheBackendInterface $cacheBackend) {
    $this->plasmicService = $plasmicService;
    $this->cacheBackend = $cacheBackend;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onRespond', -100];
    return $events;
  }

  /**
   * Responds to kernel response events.
   *
   * @param ResponseEvent $event
   *   The response event.
   */
  public function onRespond(ResponseEvent $event) {
    $request = $event->getRequest();
    if ($request->attributes->get('_route') === 'entity.plasmic_full_page.canonical') {
      $entity = $request->attributes->get('plasmic_full_page');
      $cache_id = 'plasmic_full_page:' . $entity->get('plasmic_component_id')->value;

      // Try to get the HTML from the cache.
      if ($cache = $this->cacheBackend->get($cache_id)) {
        $html = $cache->data;
      }
      else {
        // Fetch the HTML from the Plasmic service if not cached.
        $body = $this->plasmicService->fetchCustomHTML($entity->get('plasmic_component_id')->value);

        //Inject scripts into <head>
        $head_script = [];
        $head_style = [];


        //Joing head_script and head_style arrays into one string
        //$head_addons = implode("\n", array_merge($head_script, $head_style));

        // Construct the full HTML page.
        $html = <<<HTML
    <!DOCTYPE html>
    <html lang="en">
    <head>
     <meta charset="UTF-8">
            <style>
        body {
          margin: 0;
        }
      </style>
    </head>
    <body>
    $body
    </body>
    </html>
HTML;


        // Cache the HTML indefinitely (or max allowed).
        $this->cacheBackend->set($cache_id, $html, CacheBackendInterface::CACHE_PERMANENT);
      }

      // Replace only the body content while retaining Drupal's <head>.
      //$original_html = $event->getResponse()->getContent();
      //$body_content = $this->plasmicService->fetchCustomHTML($entity->get('plasmic_component_id')->value);

      //$full_html = $this->replaceBodyContent($original_html, $body_content);
      //$response = new Response($full_html);
      $response = new Response($html);
      $event->setResponse($response);
    }
  }

  /**
   * Replaces the body content in the original HTML with new content.
   *
   * @param string $original_html
   *   The original HTML page.
   * @param string $new_body_content
   *   The new body content to inject.
   *
   * @return string
   *   The modified HTML page with the new body content.
   */
  protected function replaceBodyContent(string $original_html, string $new_body_content): string {
    return preg_replace('/<body[^>]*>.*<\/body>/is', "<body>\n{$new_body_content}\n</body>", $original_html);
  }
}
