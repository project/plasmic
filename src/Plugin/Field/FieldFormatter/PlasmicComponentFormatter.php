<?php

namespace Drupal\plasmic\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\plasmic\PlasmicService;

/**
 * Plugin implementation for the Plasmic component field formatter.
 *
 * @FieldFormatter(
 *   id = "plasmic_component_default",
 *   label = @Translation("Default Plasmic Component Formatter"),
 *   field_types = {
 *     "plasmic_component"
 *   }
 * )
 */
class PlasmicComponentFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The Plasmic service.
   *
   * @var \Drupal\plasmic\PlasmicService
   */
  protected $plasmicService;

  /**
   * Constructs a PlasmicComponentFormatter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param \Drupal\plasmic\PlasmicService $plasmic_service
   *   The plasmic service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, PlasmicService $plasmic_service ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->plasmicService = $plasmic_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('plasmic.plasmic_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $html = $this->plasmicService->fetchCustomHTML($item->component_name, $item->mode,
        $item->hydrate,
        $item->embed_hydrate,
        $item->component_props);
      $elements[$delta] = [
        '#markup' =>  Markup::create($html),
      ];
    }
    return $elements;
  }
}
