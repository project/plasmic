<?php

namespace Drupal\plasmic\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Plugin implementation of the 'plasmic_component' field type.
 *
 * @FieldType(
 *   id = "plasmic_component",
 *   label = @Translation("Plasmic Component"),
 *   default_formatter = "plasmic_component_default",
 *   default_widget = "plasmic_component_default",
 * )
 */
class PlasmicComponentItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['component_name'] = DataDefinition::create('string')
      ->setLabel(t('Component Name'));
    $properties['mode'] = DataDefinition::create('string')
      ->setLabel(t('Mode'));
    $properties['hydrate'] = DataDefinition::create('boolean')
      ->setLabel(t('Hydrate'));
    $properties['embed_hydrate'] = DataDefinition::create('boolean')
      ->setLabel(t('Embed Hydrate'));
    $properties['component_props'] = MapDataDefinition::create()
      ->setLabel(t('Component Properties'))
      ->setDescription(t('A map of slot names and values for the Plasmic component.'));


    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'component_name' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'mode' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'hydrate' => [
          'type' => 'int',
          'size' => 'tiny',
        ],
        'embed_hydrate' => [
          'type' => 'int',
          'size' => 'tiny',
        ],
        'component_props' => [
          'type' => 'blob',
          'size' => 'big',
          'serialize' => TRUE,
        ],
      ],
      'indexes' => [],
      'foreign keys' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('component_name')->getValue();
    return $value === NULL || $value === '';
  }
}
