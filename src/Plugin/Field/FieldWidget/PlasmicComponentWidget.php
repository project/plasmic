<?php

namespace Drupal\plasmic\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * A widget for plasmic_component field.
 *
 * @FieldWidget(
 *   id = "plasmic_component_default",
 *   label = @Translation("Textfield"),
 *   field_types = {
 *     "plasmic_component"
 *   }
 * )
 */
class PlasmicComponentWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['component_name'] = [
      '#type' => 'textfield',
      '#title' => t('Component Name'),
      '#default_value' => isset($items[$delta]->component_name) ? $items[$delta]->component_name : '',
      '#size' => 60,
      '#maxlength' => 255,
      '#placeholder' => t('Enter Plasmic Component Name'),
    ];

    $element['mode'] = [
      '#type' => 'select',
      '#title' => t('Mode'),
      '#options' => [
        'published' => t('Published'),
        'preview' => t('Preview')
      ],
      '#default_value' => isset($items[$delta]->mode) ? $items[$delta]->mode : 'preview',
    ];

    $element['hydrate'] = [
      '#type' => 'checkbox',
      '#title' => t('Hydrate'),
      '#default_value' => isset($items[$delta]->hydrate) ? $items[$delta]->hydrate : 1,
    ];

    $element['embed_hydrate'] = [
      '#type' => 'checkbox',
      '#title' => t('Embed Hydrate'),
      '#default_value' => isset($items[$delta]->embed_hydrate) ? $items[$delta]->embed_hydrate : 1,
    ];

    // Setup for handling unlimited key/value pairs for componentProps
    $component_props = isset($items[$delta]->component_props) ? $items[$delta]->component_props : [];
    $num_slots = $form_state->get(['num_slots', $delta]) ?? count($component_props) ?: 1;
    $num_slots++;
    $form_state->set(['num_slots', $delta], $num_slots);


    $element['slots'] = [
      '#tree' => TRUE,
      '#prefix' => '<div id="slots-wrapper">',
      '#suffix' => '</div>',
    ];
    $i = 0;
    foreach ($component_props as $name => $value) {
      $element['slots'][$i] = [
        '#type' => 'container',
        'slot_name' => [
          '#type' => 'textfield',
          '#title' => t('Slot Name'),
          '#default_value' => $name,
          '#size' => 60,
          '#maxlength' => 255,
          '#placeholder' => t('Enter slot name'),
        ],
        'slot_value' => [
          '#type' => 'textfield',
          '#title' => t('Slot Value'),
          '#default_value' => $value,
          '#size' => 60,
          '#maxlength' => 255,
          '#placeholder' => t('Enter slot value'),
        ],
      ];
      $i++;
    }

    // If adding new empty slots
    for ($j = $i; $j < $num_slots; $j++) {
      $element['slots'][$j] = [
        '#type' => 'container',
        'slot_name' => [
          '#type' => 'textfield',
          '#title' => t('Slot Name'),
          '#default_value' => '',
          '#size' => 60,
          '#maxlength' => 255,
          '#placeholder' => t('Enter slot name'),
        ],
        'slot_value' => [
          '#type' => 'textfield',
          '#title' => t('Slot Value'),
          '#default_value' => '',
          '#size' => 60,
          '#maxlength' => 255,
          '#placeholder' => t('Enter slot value'),
        ],
      ];
    }

    $element['slots']['actions'] = [
      '#type' => 'actions',
      'add_name' => [
        '#type' => 'submit',
        '#value' => t('Add more'),
        '#submit' => ['\Drupal\plasmic\Plugin\Field\FieldWidget\PlasmicComponentWidget::addMore'],
        '#ajax' => [
          'callback' => '\Drupal\plasmic\Plugin\Field\FieldWidget\PlasmicComponentWidget::addMoreAjax',
          'wrapper' => 'slots-wrapper',
        ],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      if (isset($value['slots']) && is_array($value['slots'])) {
        // Initialize the component_props array.
        $value['component_props'] = [];
        foreach ($value['slots'] as $delta => $slot) {
          if (!empty($slot['slot_name']) && !empty($slot['slot_value'])) {
            // Use slot_name as the key for the array and slot_value as its value.
            $value['component_props'][$slot['slot_name']] = $slot['slot_value'];
          }
        }
        // Remove the slots structure since it's not a direct field column.
        unset($value['slots']);
      }
    }
    return $values;
  }


  /**
   * Submit handler for the "add more" button.
   */
  public static function addMore(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $delta = $triggering_element['#array_parents'][1];
    $num_slots = $form_state->get(['num_slots', $delta]);
    $form_state->set(['num_slots', $delta], ++$num_slots);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax callback for adding more slots.
   */
  public static function addMoreAjax(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $delta = $triggering_element['#array_parents'][1];
    return $form['plasmic_component'][$delta]['slots'];
    //return $form['plasmic_component']['widget'][0]['slots'];
  }
}
