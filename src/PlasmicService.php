<?php

namespace Drupal\plasmic;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Service to handle Plasmic API interactions.
 */
class PlasmicService {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new PlasmicService.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Fetch HTML from Plasmic API based on component name.
   *
   * @param string $componentName
   *   The name of the Plasmic component.
   *
   * @return string
   *   The HTML content from Plasmic.
   */
  public function fetchCustomHTML($componentName, $mode = 'preview', $hydrate = 1, $embedHydrate = 1, $slotContents = []) {

    $config = \Drupal::config('plasmic.settings');
    $projectId = $config->get('project_id');
    $apiToken = $config->get('api_token');

    // Prepare componentProps to override the button text
    //$componentProps = json_encode(['bigtitle' => 'New Button Text', 'subtitle' => 'Subtitle']);

    // Prepare the base URL
    $url = "https://codegen.plasmic.app/api/v1/loader/html/{$mode}/{$projectId}/{$componentName}?hydrate={$hydrate}&embedHydrate={$embedHydrate}";

    // If $slotContents is provided and not empty, include it as componentProps
    if (!empty($slotContents)) {
      $componentProps = json_encode($slotContents);
      $url .= '&componentProps=' . urlencode($componentProps);
    }

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, [
      "x-plasmic-api-project-tokens: {$projectId}:{$apiToken}"
    ]);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($curl);
    curl_close($curl);

    $result = json_decode($response);
    if(isset($result->html)){
      return $result->html;
    }
    elseif(isset($result->error)){
      \Drupal::logger('plasmic')->error(print_r($result->error,TRUE));
    }
    return 'Failed to fetch HTML.';
  }
}
