<?php

namespace Drupal\plasmic\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

class PlasmicContentForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Your custom submission logic here
    $entity = $this->entity;
    $status = $entity->save();

    // Status message
    if ($status == SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The Plasmic Content has been created.'));
    } else {
      $this->messenger()->addMessage($this->t('The Plasmic Content has been updated.'));
    }

    // Redirect to a specific route after save
    $form_state->setRedirect('plasmic.admin_content');
  }
}
