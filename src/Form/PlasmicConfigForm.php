<?php

namespace Drupal\plasmic\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;



class PlasmicConfigForm extends ConfigFormBase {


  /**
   * The Plasmic cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Constructs a new PlasmicConfigForm.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   The Plasmic cache backend.
   */
  public function __construct(CacheBackendInterface $cacheBackend) {
    $this->cacheBackend = $cacheBackend;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.plasmic')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['plasmic.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'plasmic_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('plasmic.settings');

    $form['project_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project ID'),
      '#default_value' => $config->get('project_id'),
      '#description' => $this->t('Enter the Plasmic Project ID.'),
    ];

    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project Public API Token'),
      '#default_value' => $config->get('api_token'),
      '#description' => $this->t('Enter the Plasmic Project Public API Token.'),
    ];

    // Add a button to clear the Plasmic cache.
    $form['clear_cache'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear Plasmic Cache'),
      '#submit' => ['::clearPlasmicCache'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('plasmic.settings')
      ->set('project_id', $form_state->getValue('project_id'))
      ->set('api_token', $form_state->getValue('api_token'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Clears the Plasmic cache.
   */
  public function clearPlasmicCache(array &$form, FormStateInterface $form_state) {
    $this->cacheBackend->deleteAll();
    $this->messenger()->addMessage($this->t('Plasmic cache cleared successfully.'));
  }
}
