## Plasmic Integration Module

### Overview

The **Plasmic Integration** module for Drupal seamlessly integrates Plasmic components into your Drupal site.
[Plasmic](https://www.plasmic.app) is a powerful visual builder for designing and creating dynamic, responsive
components and pages. With this module, you can embed Plasmic components either as full pages, content pages or
as fields within your content types and custom blocks, enabling a streamlined, visually-driven development process.

### Features

- **Custom Field Type**: Add Plasmic components to any Drupal entity with our custom `plasmic_component` field.
- **Dynamic Properties**: Easily configure component properties such as mode (published/preview), hydration, and embedded hydration.
- **Flexible Slot Management**: Define unlimited key-value pairs for component slots, dynamically added through the form interface, easily passing content from Drupal CMS into Plasmic componet
- **Entity Integration**: Support for Plasmic Full Page and Plasmic Content entities, enabling you to use Plasmic for both full-page layouts and individual content pieces.
- **Custom Blocks**: Create custom Plasmic blocks available in the Custom Block Library, enabling you to add Plasmic components as Drupal blocks (eg: header/footer,etc...)
- **Cache Management**: Integrated caching for Plasmic HTML output to optimize performance, with an admin option to clear the cache.

### Installation

1. **Download and Enable**:
  - Download the module and place it in your Drupal installation under `modules/custom`.
  - Enable the module via the Drupal admin UI or using Drush: `drush en plasmic -y`.

2. **Configuration**:
  - Navigate to the module configuration page at `/admin/config/system/plasmic`.
  - Enter your Plasmic Project ID and Public API Token.

3. **Adding Fields**:
  - Add the `plasmic_component` field to any content type or custom block.
  - Configure the field settings to specify component properties and slot values.

4. **Creating Entities**:
  - Create Plasmic Full Page or Plasmic Content entities from the `admin/content/plasmic` page.

### Usage

1. **Embedding Components**:
  - When adding or editing content, fill in the Plasmic component field with the required component name and properties.
  - Use the dynamic slot fields to provide values for your Plasmic component slots.

2. **Custom Blocks**:
  - Create custom blocks with embedded Plasmic components to use across various regions of your site.

### Contribution

We welcome contributions from the community! If you encounter any issues or have suggestions for improvements, please create an issue in the [issue queue](https://www.drupal.org/project/issues/plasmic).

### Maintainers

- [Tamer Zoubi](https://www.drupal.org/u/tamerzg)
